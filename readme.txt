In order to enable guvnor to collect enumerators via opencds_apelon service you should add opencds_common.jar om the same folder where Dockerfile is located

Build image :
sudo docker build -t="opencds/guvnor" .

Create container, examples :
sudo docker run -p 28080:8080 --name guvnor --link opencds_apelon:oa -i -t opencds/guvnor
sudo docker run -d -p 28080:8080 --name guvnor --link opencds_apelon:oa opencds/guvnor
sudo docker run -d -p 28080:8080 --name guvnor --link opencds_apelon:oa opencds/guvnor

You must be able to see guvnor in the following link
http://localhost:28080/drools-guvnor

